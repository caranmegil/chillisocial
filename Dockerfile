FROM node:16

# Web clinet installs
WORKDIR /usr/src/chillisocial/web
COPY ./web/package*.json ./
RUN npm ci

# server installs
WORKDIR /usr/src/chillisocial
COPY package*.json ./
RUN npm ci

# source
COPY . .

# web client build
WORKDIR /usr/src/chillisocial/web
RUN npm run build

# entry
WORKDIR /usr/src/chillisocial
EXPOSE 443 80
CMD [ "node", "index.js" ]
